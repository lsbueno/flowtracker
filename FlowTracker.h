/* 
 * File:   FlowTracker.h
 * Author: leonardo
 *
 * Created on July 19, 2014, 9:26 AM
 */

#ifndef FLOWTRACKER_H
#define	FLOWTRACKER_H

#include "ImageSource.h"
#include "ViperRDF.h"

#include "opencv2/core/core.hpp"
#include "boost/program_options.hpp"
#include <vector>

namespace po = boost::program_options;

namespace tracker {

    static const int OUTPUT_MODE_DEBUG = 1;
    static const int OUTPUT_MODE_DISPLAY = 2;
    static const int OUTPUT_MODE_RECORD = 3;

    extern bool debug_enabled;

    class FlowTracker {
    public:
        FlowTracker(ImageSource& src, po::variables_map& params);
        FlowTracker(const FlowTracker& orig);
        virtual ~FlowTracker();
        bool track();
        void writeViperFile(std::string filename);
        void record();
        void saveImage(cv::Mat& frame);
        void display(
                cv::Mat& baseImage,
                const std::vector<uint>& labels,
                const std::vector<std::vector <cv::Point>>&contours,
                const std::vector<cv::Rect>& bounding_rects,
                const std::vector<cv::Point>& centroids,
                const std::vector<cv::Point>& velocities,
                const std::vector<int>& missing,
                const std::vector<int>& frame_count);
        void debug(
                const cv::Mat& prevGrey,
                const cv::Mat& prevFrame,
                const cv::Mat& grey,
                const cv::Mat& frame,
                const cv::Mat& stabGrey,
                const cv::Mat& residualFlow,
                const cv::Mat& residualFlowIntensity,
                const cv::Mat& flowIntensity,
                const cv::Mat& trans,
                const std::vector<uint>& labels,
                const std::vector<std::vector<cv::Point>>&contours,
                const std::vector<cv::Point>& centroids,
                const std::vector<cv::Rect>& bounding_rects,
                const std::vector<cv::Point>& velocities,
                const std::vector<int>& missing,
                const std::vector<int>& frame_count,
                const std::vector<std::vector <cv::Point>>&candidate_contours,
                const std::vector<cv::Rect>& candidate_bounding_rects,
                const std::vector<cv::Point>& candidate_centroids,
                const std::vector<cv::Point>& candidate_velocities);
    private:
        void estimateResidualFlow(const cv::Mat& prevGrey,
                const cv::Mat& stabGrey,
                cv::Mat& residualFlow);
        //Inputs
        ImageSource& image_src;
        po::variables_map& params;
        const int stab_trans_pyr_levels;
        const int stab_trans_max_feature_count;
        const int stab_trans_grid_rows;
        const int stab_trans_grid_cols;
        const int stab_trans_margin;
        const double stab_trans_qlevel;
        const double stab_trans_min_dist;
        const cv::Size stab_trans_win_size;
        const double residual_motion_pyr_scale;
        const int residual_motion_levels;
        const int residual_motion_win_size;
        const int residual_motion_iterations;
        const int residual_motion_poly_n;
        const double residual_motion_poly_sigma;
        const int residual_motion_flags;
        const double flow_denoise_threshold;
        const cv::Size flow_denoise_openclose_kernel_size;
        const int moving_blobs_roi_margin;
        const int moving_blobs_min_area_threshold;
        const int moving_blobs_max_area_threshold;
        const std::string target_assoc_algorithm;
        const float target_assoc_gate;
        const int missing_threshold;
        const int output_mode;
        const std::string output_image_dir;
        const bool save_image;

        //Internal state
        bool initialize_motion_mask;

        cv::Mat prevGrey, grey, prevFrame, frame, stabGrey, stabFrame;

        cv::Mat referencePoints;

        cv::Mat transform;

        cv::Mat residualFlow;

        cv::Mat residualFlowIntensity, denoisedResidualFlowIntensity, motion_mask;

        uint processed_frames;

        std::string source;

        std::string residual_motion_algorithm;

        uint framerate;

        uint waitKey;


        //Iteration Outputs
        std::vector<uint> labels;
        std::vector<std::vector<cv::Point>> contours;
        std::vector<cv::Rect> bounding_rects;
        std::vector<cv::Point> centroids;
        std::vector<cv::Point> velocity;
        //contador de frames em que o objeto está aparecendo
        std::vector<int> frame_count;
        //contador de frames em que o objeto está missing
        std::vector<int> missing;

        ViperRDF *viperRDF;
    };

    void create_ref_points(
            cv::Mat& referencePoints,
            const cv::Mat& motion_mask,
            const cv::Mat & image,
            const int maxFeatureCount,
            const int gridRows = 4,
            const int gridCols = 4,
            const int margin = 20,
            const double qlevel = 0.01,
            const double minDist = 1.0);

    void estimate_transform(cv::Mat& t, const cv::Mat& desired_reference_points, cv::Mat& reference_points, const cv::Mat& prevGrey, const cv::Mat& grey, const cv::Size winSize = cv::Size(21, 21), const int maxLevel = 3);

    void lk_estimate_flow_points(const cv::Mat& prevGrey, const cv::Mat& grey, const cv::Mat& desiredReferencePoints, cv::Mat& referencePoints, cv::Mat& flowPoints, const cv::Size winSize = cv::Size(21, 21), const int maxLevel = 3);

    void to_grey(cv::Mat& frame, cv::Mat& grey);

    void calculate_flow_intensity(const cv::Mat& residualFlow, cv::Mat& residualFlowIntensity);

    void remove_flowintensity_noise(const cv::Mat& flowIntensity, cv::Mat& denoisedFlowIntensity, const double threshold = 1., cv::Size openCloseKernelSize = cv::Size(15, 15));

    void create_contour_bounding_rects(std::vector<std::vector <cv::Point>>&contours, std::vector<cv::Rect>& bounding_rects);

    void filter_non_object_like_blobs(std::vector<std::vector <cv::Point>>&candidate_contours, const cv::Mat stabFrame, const int moving_blobs_min_area_threshold, const int moving_blobs_max_area_threshold);

    void detect_moving_candidates(const cv::Mat& residualFlow, const cv::Mat& residualFlowIntensity, std::vector<std::vector <cv::Point>>&candidate_contours, std::vector<cv::Rect>& candidate_bounding_rects, std::vector<cv::Point>& candidate_centroids, std::vector<cv::Point>& candidate_velocities, const cv::Mat& motion_mask);

    void update_state_when_transform_invalid(
            std::vector<uint>& labels,
            std::vector<std::vector<cv::Point>>&contours,
            std::vector<cv::Point>& centroids,
            std::vector<cv::Rect>& bounding_rects,
            std::vector<cv::Point>& velocities,
            std::vector<int>& missing,
            std::vector<int>& frame_count,
            int missing_threshold
            );

    void nearest_neighbor_associate(
            std::vector<uint>& prev_labels,
            std::vector<std::vector<cv::Point>>&prev_contours,
            std::vector<cv::Point>& prev_centroids,
            std::vector<cv::Rect>& prev_bounding_rects,
            std::vector<cv::Point>& prev_velocity,
            std::vector<int>& prev_missing,
            std::vector<int>& prev_frame_count,
            std::vector<std::vector <cv::Point>>&candidate_contours,
            std::vector<cv::Rect>& candidate_bounding_rects,
            std::vector<cv::Point>& candidate_centroids,
            std::vector<cv::Point>& candidate_velocities,
            const cv::Mat& motion_mask,
            std::vector<bool>& match_mask,
            std::vector<uint>& new_labels,
            std::vector<std::vector<cv::Point>>&new_contours,
            std::vector<cv::Point>& new_centroids,
            std::vector<cv::Rect>& new_bounding_rects,
            std::vector<cv::Point>& new_velocity,
            std::vector<int>& new_missing,
            std::vector<int>& new_frame_count,
            const float target_assoc_gate,
            const int missing_threshold);

    void nearest_neighbor_initialize(
            cv::Mat& residualFlowIntensity,
            std::vector<uint>& prev_labels,
            std::vector<std::vector<cv::Point>>&prev_contours,
            std::vector<cv::Point>& prev_centroids,
            std::vector<cv::Rect>& prev_bounding_rects,
            std::vector<cv::Point>& prev_velocity,
            std::vector<int>& prev_missing,
            std::vector<int>& prev_frame_count,
            std::vector<std::vector <cv::Point>>&candidate_contours,
            std::vector<cv::Rect>& candidate_bounding_rects,
            std::vector<cv::Point>& candidate_centroids,
            std::vector<cv::Point>& candidate_velocities,
            const cv::Mat& motion_mask,
            std::vector<bool>& match_mask,
            std::vector<uint>& next_labels,
            std::vector<std::vector<cv::Point>>&next_contours,
            std::vector<cv::Point>& next_centroids,
            std::vector<cv::Rect>& next_bounding_rects,
            std::vector<cv::Point>& next_velocities,
            std::vector<int>& next_missing,
            std::vector<int>& next_frame_count,
            const int moving_blobs_min_area_threshold,
            const int moving_blobs_max_area_threshold,
            const int moving_blobs_roi_margin
            );


    uint create_new_label();

    void drawMovingObjects(cv::Mat& baseImage, const std::vector<uint>& labels, const std::vector<std::vector <cv::Point>>&contours, const std::vector<cv::Rect>& bounding_rects, const std::vector<cv::Point>& centroids, const std::vector<cv::Point>& velocities, const std::vector<int>& missing, const std::vector<int>& prev_frame_count);

    cv::Mat& drawMotion(const cv::Mat transform, const cv::Mat& residual_flow, cv::Mat& motion_image, int step, const cv::Scalar& color, double threshold);

    cv::Mat& drawStabDiff(const cv::Mat& prevGrey, const cv::Mat& stab_grey, cv::Mat& stab_diff);

    bool transformSeemsValid(const cv::Mat& trans);

    cv::Mat& drawDenseFlow(const cv::Mat& flow, int step, const cv::Scalar& color, cv::Mat& flow_image, double threshold);

    void drawRoiCaption(cv::Mat& roi, const cv::Rect roiRect, const cv::Point& p, const std::string& caption);

    void drawCaption(cv::Mat& image, const cv::Point& p, const std::string& caption);

    std::string transDescription(const cv::Mat& trans);

    cv::Mat& drawFlowIntensity(const cv::Mat& flowIntensity, cv::Mat& flow_image, bool normalization, bool equalization, bool applyColormap);

    void drawReferencePoints(const cv::Mat& referencePoints, const cv::Scalar& color, cv::Mat& flow_image);

    //    void arrowedLine(cv::Mat& img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
    //            int thickness = 2, int lineType = 8, int shift = 0, double normalizationFactor = 8);

    void debug(const std::string msg, const cv::Mat& m);

    template <class type> void debug(const std::string msg, const std::vector<type>& v);

}
#endif	/* FLOWTRACKER_H */

