/* 
 * File:   JonkerVolgenantDataAssociation.h
 * Author: leonardo
 *
 * Created on August 1, 2014, 2:59 PM
 */

#ifndef JONKERVOLGENANTDATAASSOCIATION_H
#define	JONKERVOLGENANTDATAASSOCIATION_H

namespace tracker {
    
    void lap(cv::Mat& costs);
    
}
#endif	/* JONKERVOLGENANTDATAASSOCIATION_H */

