/* 
 * File:   ImageSource.h
 * Author: leonardo
 *
 * Created on July 19, 2014, 9:27 AM
 */

#ifndef IMAGESOURCE_H
#define	IMAGESOURCE_H

#include <opencv2/core/core.hpp>

namespace tracker {

    class ImageSource {
    public:

        ImageSource() {
        };

        virtual ~ImageSource() {
        };
        virtual std::string get_current_filename() = 0;
        virtual uint get_current_index() = 0;
        virtual cv::Mat next() = 0;
        virtual uint count() = 0;
    private:

    };

}
#endif	/* IMAGESOURCE_H */

