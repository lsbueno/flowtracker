/* 
 * File:   FlowTracker.cpp
 * Author: leonardo
 * 
 * Created on July 19, 2014, 9:26 AM
 */

#include "FlowTracker.h"
#include "hungarian_association.h"
#include "ViperRDF.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/video.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/contrib/contrib.hpp>
#include <string>
#include <algorithm>
#include <exception>
#include <stdexcept>
#include <boost/format.hpp>
#include <opencv2/gpu/gpu.hpp>
#include <boost/filesystem/path.hpp>

namespace po = boost::program_options;

namespace tracker {

    bool debug_enabled = false;

    int to_int_output_mode(std::string mode) {
        if (mode.compare("debug") == 0) {
            return OUTPUT_MODE_DEBUG;
        } else if (mode.compare("display") == 0) {
            return OUTPUT_MODE_DISPLAY;
        } else {
            return OUTPUT_MODE_RECORD;
        }
    }

    FlowTracker::FlowTracker(ImageSource& src, po::variables_map& params) :
    image_src(src),
    params(params),
    stab_trans_pyr_levels(params["stab_trans_pyr_levels"].as<int>()),
    stab_trans_max_feature_count(params["stab_trans_max_feature_count"].as<int>()),
    stab_trans_grid_rows(params["stab_trans_grid_rows"].as<int>()),
    stab_trans_grid_cols(params["stab_trans_grid_cols"].as<int>()),
    stab_trans_margin(params["stab_trans_margin"].as<int>()),
    stab_trans_qlevel(params["stab_trans_qlevel"].as<double>()),
    stab_trans_min_dist(params["stab_trans_min_dist"].as<double>()),
    stab_trans_win_size(cv::Size(params["stab_trans_win_size"].as<int>(), params["stab_trans_win_size"].as<int>())),
    residual_motion_pyr_scale(params["residual_motion_pyr_scale"].as<double>()),
    residual_motion_levels(params["residual_motion_levels"].as<int>()),
    residual_motion_win_size(params["residual_motion_win_size"].as<int>()),
    residual_motion_iterations(params["residual_motion_iterations"].as<int>()),
    residual_motion_poly_n(params["residual_motion_poly_n"].as<int>()),
    residual_motion_poly_sigma(params["residual_motion_poly_sigma"].as<double>()),
    residual_motion_flags(params["residual_motion_flags"].as<int>()),
    flow_denoise_threshold(params["flow_denoise_threshold"].as<double>()),
    flow_denoise_openclose_kernel_size(cv::Size(params["flow_denoise_openclose_kernel_size"].as<int>(), params["flow_denoise_openclose_kernel_size"].as<int>())),
    moving_blobs_roi_margin(params["moving_blobs_roi_margin"].as<int>()),
    moving_blobs_min_area_threshold(params["moving_blobs_min_area_threshold"].as<int>()),
    moving_blobs_max_area_threshold(params["moving_blobs_max_area_threshold"].as<int>()),
    target_assoc_algorithm(params["target_assoc_algorithm"].as<std::string>()),
    target_assoc_gate(params["target_assoc_gate"].as<float>()),
    missing_threshold(params["missing_threshold"].as<int>()),
    output_mode(to_int_output_mode(params["output_mode"].as<std::string>())),
    output_image_dir(params["output_image_dir"].as<std::string>()),
    save_image(params["output_image_dir"].as<std::string>() != ""),
    initialize_motion_mask(true),
    processed_frames(0),
    source(params["source"].as<std::string>()),
    residual_motion_algorithm(params["residual_motion_algorithm"].as<std::string>()),
    framerate(params["framerate"].as<uint>()),
    waitKey(params["waitKey"].as<int>()),
    viperRDF(NULL) {
        //get first reference frame and obtain frame size and type info
        prevFrame = image_src.next();
        if (prevFrame.data == NULL) {
            throw std::runtime_error("couldn't read initial frame");
        }
        to_grey(prevFrame, prevGrey);
        processed_frames++;

    }

    FlowTracker::~FlowTracker() {
        if (viperRDF) {
            delete viperRDF;
        }
    }

    bool FlowTracker::track() {
        //get the next frame and converts to grey
        frame = image_src.next();
        if (frame.data == NULL) {
            return false;
        }

        to_grey(frame, grey);

        if (prevGrey.size() != grey.size() || grey.channels() != 1) {
            throw std::runtime_error(
                    boost::str(
                    boost::format("bad frame read. Expected frame with size (%1,%2) and channels == 1 but found image with size (%3,%4) and channels %5")
                    % prevGrey.rows % prevGrey.cols % grey.rows % grey.cols % grey.channels())
                    );
        }

        cv::Mat desiredReferencePoints;
        std::vector<std::vector < cv::Point>> candidate_contours;
        std::vector<cv::Rect> candidate_bounding_rects;
        std::vector<cv::Point> candidate_centroids;
        std::vector<cv::Point> candidate_velocities;
        std::vector<bool> match_mask;
        cv::Mat temp_mask;
        cv::Mat debugImage;

        std::vector<uint> new_labels;
        std::vector<std::vector < cv::Point>> new_contours;
        std::vector<cv::Point> new_centroids;
        std::vector<cv::Rect> new_bounding_rects;
        std::vector<cv::Point> new_velocity;
        std::vector<int> new_missing;
        std::vector<int> new_frame_count;

PRE_CREATE_REF_POINTS:
        create_ref_points(
                desiredReferencePoints,
                motion_mask,
                prevGrey,
                stab_trans_max_feature_count,
                stab_trans_grid_rows,
                stab_trans_grid_cols,
                stab_trans_margin,
                stab_trans_qlevel,
                stab_trans_min_dist);

        if (!motion_mask.empty() && debug_enabled) {
            cv::cvtColor(motion_mask, debugImage, CV_GRAY2BGR);
            drawReferencePoints(desiredReferencePoints, cv::Scalar(127, 255, 127), debugImage);
            cv::imshow("motion_mask", debugImage);
        }

        estimate_transform(transform, desiredReferencePoints, referencePoints, prevGrey, grey, stab_trans_win_size, stab_trans_pyr_levels);

        //Estabiliza o frame atual para alinhá-lo com o frame anterior
        //TODO: Parametrizar o tipo de interpolação

        if (transformSeemsValid(transform)) {


            cv::warpAffine(grey, stabGrey, transform, grey.size(), cv::INTER_LINEAR + cv::WARP_INVERSE_MAP, cv::BORDER_TRANSPARENT);
            cv::warpAffine(frame, stabFrame, transform, frame.size(), cv::INTER_LINEAR + cv::WARP_INVERSE_MAP, cv::BORDER_TRANSPARENT);

            //Calcula o fluxo residual. O fluxo obtido abaixo já desconsidera o 
            //movimento da camera, pois as imagens já foram estabilizadas
            //TODO: Implementar usando Brox e GPU
            estimateResidualFlow(prevGrey,
                    stabGrey,
                    residualFlow);

            calculate_flow_intensity(residualFlow, residualFlowIntensity);

            remove_flowintensity_noise(residualFlowIntensity, denoisedResidualFlowIntensity, flow_denoise_threshold, flow_denoise_openclose_kernel_size);

            denoisedResidualFlowIntensity.convertTo(temp_mask, CV_8UC1, 255);

            cv::threshold(temp_mask, motion_mask, 1, 255, cv::THRESH_BINARY_INV);

            if (initialize_motion_mask) {
                candidate_contours.clear();
                initialize_motion_mask = false;
                goto PRE_CREATE_REF_POINTS;
            }

            bounding_rects.clear();

            detect_moving_candidates(
                    residualFlow,
                    denoisedResidualFlowIntensity,
                    candidate_contours,
                    candidate_bounding_rects,
                    candidate_centroids,
                    candidate_velocities,
                    motion_mask);

            bounding_rects = candidate_bounding_rects;

            nearest_neighbor_associate(labels,
                    contours,
                    centroids,
                    bounding_rects,
                    velocity,
                    missing,
                    frame_count,
                    candidate_contours,
                    candidate_bounding_rects,
                    candidate_centroids,
                    candidate_velocities,
                    motion_mask,
                    match_mask,
                    new_labels,
                    new_contours,
                    new_centroids,
                    new_bounding_rects,
                    new_velocity,
                    new_missing,
                    new_frame_count,
                    target_assoc_gate,
                    missing_threshold);

            nearest_neighbor_initialize(
                    denoisedResidualFlowIntensity,
                    labels,
                    contours,
                    centroids,
                    bounding_rects,
                    velocity,
                    missing,
                    frame_count,
                    candidate_contours,
                    candidate_bounding_rects,
                    candidate_centroids,
                    candidate_velocities,
                    motion_mask,
                    match_mask,
                    new_labels,
                    new_contours,
                    new_centroids,
                    new_bounding_rects,
                    new_velocity,
                    new_missing,
                    new_frame_count,
                    moving_blobs_min_area_threshold,
                    moving_blobs_max_area_threshold,
                    moving_blobs_roi_margin
                    );

            labels = new_labels;
            contours = new_contours;
            centroids = new_centroids;
            bounding_rects = new_bounding_rects;
            velocity = new_velocity;
            missing = new_missing;
            frame_count = new_frame_count;
        } else {
            update_state_when_transform_invalid(
                    labels,
                    contours,
                    centroids,
                    bounding_rects,
                    velocity,
                    missing,
                    frame_count,
                    missing_threshold);
        }

        cv::Mat flowIntensity;
        cv::Mat flow;

        switch (output_mode) {
            case OUTPUT_MODE_DEBUG:
                estimateResidualFlow(prevGrey, grey, flow);
                calculate_flow_intensity(flow, flowIntensity);
                debug(
                        prevGrey,
                        prevFrame,
                        grey,
                        frame,
                        stabGrey,
                        residualFlow,
                        denoisedResidualFlowIntensity,
                        flowIntensity,
                        transform,
                        labels,
                        contours,
                        centroids,
                        bounding_rects,
                        velocity,
                        missing,
                        frame_count,
                        candidate_contours,
                        candidate_bounding_rects,
                        candidate_centroids,
                        candidate_velocities);
                break;
            case OUTPUT_MODE_DISPLAY:
                display(frame, labels, contours, bounding_rects, centroids, velocity, missing, frame_count);
                break;
            default: // OUTPUT_MODE_RECORD:
                record();
        }


        std::swap(prevGrey, grey);
        std::swap(prevFrame, frame);
        processed_frames++;
        if (output_mode != OUTPUT_MODE_RECORD) {
            cv::waitKey(waitKey);
        }
        return true;
    }

    void FlowTracker::record() {
        if (!viperRDF) {
            viperRDF = new ViperRDF("viper_data.xml", source, framerate);
        }
        viperRDF->addFrameTracks(labels, bounding_rects, missing);
    }

    void FlowTracker::saveImage(cv::Mat& frame) {
        boost::filesystem::path dirpath(output_image_dir);
        boost::filesystem::path source_file(image_src.get_current_filename());
        cv::imwrite((dirpath / source_file.filename()).native(), frame);
    }

    void FlowTracker::estimateResidualFlow(const cv::Mat& prevGrey,
            const cv::Mat& stabGrey,
            cv::Mat& residualFlow) {
        if ("brox" == residual_motion_algorithm) {
            cv::Mat prevFloat;
            cv::Mat stabFloat;
            prevGrey.convertTo(prevFloat, CV_32F, 1.0 / 255.0);
            stabGrey.convertTo(stabFloat, CV_32F, 1.0 / 255.0);
            cv::gpu::GpuMat gpuPrev(prevFloat);
            cv::gpu::GpuMat gpuStab(stabFloat);
            cv::gpu::GpuMat gpuResidualX;
            cv::gpu::GpuMat gpuResidualY;
            //cv::gpu::OpticalFlowDual_TVL1_GPU brox;
            cv::gpu::BroxOpticalFlow brox(0.197f, 50.0f, 0.8f, 10, 77, 10);
            brox(gpuPrev, gpuStab, gpuResidualX, gpuResidualY);
            std::vector<cv::gpu::GpuMat> residualChannels;
            residualChannels.push_back(gpuResidualX);
            residualChannels.push_back(gpuResidualY);
            cv::gpu::GpuMat gpuResidualFlow;
            cv::gpu::merge(residualChannels, gpuResidualFlow);
            gpuResidualFlow.download(residualFlow);
        } else {
            cv::calcOpticalFlowFarneback(
                    prevGrey,
                    stabGrey,
                    residualFlow,
                    residual_motion_pyr_scale,
                    residual_motion_levels,
                    residual_motion_win_size,
                    residual_motion_iterations,
                    residual_motion_poly_n,
                    residual_motion_poly_sigma,
                    residual_motion_flags);

        }

    }

    void FlowTracker::writeViperFile(std::string filename) {
        viperRDF->writeTo(filename);
        delete viperRDF;
        viperRDF = NULL;
    }

    void update_state_when_transform_invalid(
            std::vector<uint>& labels,
            std::vector<std::vector<cv::Point>>&contours,
            std::vector<cv::Point>& centroids,
            std::vector<cv::Rect>& bounding_rects,
            std::vector<cv::Point>& velocities,
            std::vector<int>& missing,
            std::vector<int>& frame_count,
            int missing_threshold
            ) {

        for (int i = contours.size() - 1; i >= 0; i--) {
            if (missing[i] >= missing_threshold) {
                if (debug_enabled) {
                    std::cout << "REMOVENDO objeto " << i << " que sumiu por " << (missing[i] + 1) << " frames." << std::endl;
                }

                labels.erase(labels.begin() + i);
                bounding_rects.erase(bounding_rects.begin() + i);
                contours.erase(contours.begin() + i);
                centroids.erase(centroids.begin() + i);
                velocities.erase(velocities.begin() + i);
                missing.erase(missing.begin() + i);
                frame_count.erase(frame_count.begin() + i);
            } else {
                //estimativa da posição assumindo velocidade constante
                centroids.at(i) += velocities[i];
                missing.at(i) += 1;
                frame_count.at(i) = 0;
                if (debug_enabled) {
                    std::cout << "Objeto " << i << " sumiu por " << missing[i] << " frames" << std::endl;
                    std::cout << "posicao: " << centroids[i].x << "," << centroids[i].y << std::endl;
                    std::cout << "velocidade: " << velocities[i].x << "," << velocities[i].y << std::endl << std::endl;
                }
            }
        }
    }

    void nearest_neighbor_associate(
            std::vector<uint>& prev_labels,
            std::vector<std::vector<cv::Point>>&prev_contours,
            std::vector<cv::Point>& prev_centroids,
            std::vector<cv::Rect>& prev_bounding_rects,
            std::vector<cv::Point>& prev_velocities,
            std::vector<int>& prev_missing,
            std::vector<int>& prev_frame_count,
            std::vector<std::vector <cv::Point>>&candidate_contours,
            std::vector<cv::Rect>& candidate_bounding_rects,
            std::vector<cv::Point>& candidate_centroids,
            std::vector<cv::Point>& candidate_velocities,
            const cv::Mat& motion_mask,
            std::vector<bool>& match_mask,
            std::vector<uint>& new_labels,
            std::vector<std::vector<cv::Point>>&new_contours,
            std::vector<cv::Point>& new_centroids,
            std::vector<cv::Rect>& new_bounding_rects,
            std::vector<cv::Point>& new_velocities,
            std::vector<int>& new_missing,
            std::vector<int>& new_frame_count,
            float target_assoc_gate,
            const int missing_threshold
            ) {

        match_mask = std::vector<bool>(candidate_contours.size(), false);
        //Não há nada para associar ou não há observações, retorna. Será tudo tratado na inicialização ou prunning
        if (prev_contours.empty()) {
            return;
        }

        //A matrix de associação, que tem a distancia entre cada um 
        //dos objetos trackeados e dos contornos localizados        
        cv::Mat A(prev_contours.size(), candidate_contours.size(), CV_32FC1);

        std::map<int, int> association;

        if (!candidate_contours.empty()) {
            for (size_t i = 0; i < prev_contours.size(); i++) {
                float cx = prev_centroids[i].x;
                float cy = prev_centroids[i].y;
                for (size_t j = 0; j < candidate_contours.size(); j++) {
                    float dx = cx - candidate_centroids[j].x;
                    float dy = cy - candidate_centroids[j].y;
                    A.at<float>(i, j) = sqrt(pow(dx, 2) + pow(dy, 2));
                }
            }
            hungarian_association(A, association);
        }

        if (debug_enabled) {
            debug("prev_labels", prev_labels);

            debug("prev_centroids", prev_centroids);

            debug("candidate_centroids", candidate_centroids);

            debug("A", A);
        }
        float distance = -1;
        int minLoc = -1;
        for (size_t i = 0; i < prev_contours.size(); i++) {
            bool trackFound = association.find(i) != association.end();
            if (trackFound) {
                minLoc = association[i];
                distance = A.at<float>(i, minLoc);
            }
            if (!trackFound || distance > target_assoc_gate) {
                if (prev_missing[i] < missing_threshold) {
                    new_labels.push_back(prev_labels[i]);
                    new_bounding_rects.push_back(prev_bounding_rects[i]);
                    new_contours.push_back(prev_contours[i]);
                    //estimativa da posição assumindo velocidade constante
                    new_centroids.push_back(prev_centroids[i] + prev_velocities[i]);
                    //assume que mantem velocidade
                    new_velocities.push_back(prev_velocities[i]);
                    new_missing.push_back(prev_missing[i] + 1);
                    new_frame_count.push_back(0);
                    if (debug_enabled) {
                        std::cout << "Objeto " << i << " sumiu por " << new_missing[i] << " frames" << std::endl;
                        std::cout << "posicao: " << new_centroids[i].x << "," << new_centroids[i].y << std::endl;
                        std::cout << "velocidade: " << new_velocities[i].x << "," << new_velocities[i].y << std::endl << std::endl;
                    }
                } else {
                    if (debug_enabled) {
                        std::cout << "REMOVENDO objeto " << i << " que sumiu por " << (prev_missing[i] + 1) << " frames." << std::endl;
                    }
                }
            } else {
                new_labels.push_back(prev_labels[i]);
                if (debug_enabled) {
                    std::cout << "Associando " << i << " com " << minLoc << std::endl;
                }
                match_mask[minLoc] = true;
                new_bounding_rects.push_back(candidate_bounding_rects[minLoc]);
                new_contours.push_back(candidate_contours[minLoc]);
                new_centroids.push_back(candidate_centroids[minLoc]);
                new_velocities.push_back(candidate_velocities[minLoc]);
                new_missing.push_back(0);
                new_frame_count.push_back(prev_frame_count[i] + 1);
            }
        }
        if (debug_enabled) {
            debug("new_centroids", new_centroids);
        }
    }

    void nearest_neighbor_initialize(
            cv::Mat& residualFlowIntensity,
            std::vector<uint>& prev_labels,
            std::vector<std::vector<cv::Point>>&prev_contours,
            std::vector<cv::Point>& prev_centroids,
            std::vector<cv::Rect>& prev_bounding_rects,
            std::vector<cv::Point>& prev_velocity,
            std::vector<int>& prev_missing,
            std::vector<int>& prev_frame_count,
            std::vector<std::vector <cv::Point>>&candidate_contours,
            std::vector<cv::Rect>& candidate_bounding_rects,
            std::vector<cv::Point>& candidate_centroids,
            std::vector<cv::Point>& candidate_velocities,
            const cv::Mat& motion_mask,
            std::vector<bool>& match_mask,
            std::vector<uint>& next_labels,
            std::vector<std::vector<cv::Point>>&next_contours,
            std::vector<cv::Point>& next_centroids,
            std::vector<cv::Rect>& next_bounding_rects,
            std::vector<cv::Point>& next_velocities,
            std::vector<int>& next_missing,
            std::vector<int>& next_frame_count,
            const int moving_blobs_min_area_threshold,
            const int moving_blobs_max_area_threshold,
            const int moving_blobs_roi_margin
            ) {

        int matched = std::count_if(match_mask.begin(), match_mask.end(), [](bool val) {
            return val;
        });

        if ((candidate_contours.size() - matched) == 0) {
            //All candidate contours have been matched
            return;
        }

        float minX = moving_blobs_roi_margin;
        float maxX = residualFlowIntensity.cols - moving_blobs_roi_margin;
        float minY = moving_blobs_roi_margin;
        float maxY = residualFlowIntensity.rows - moving_blobs_roi_margin;

        for (size_t j = 0; j < candidate_contours.size(); j++) {
            if (!match_mask[j]) {
                float cx = candidate_centroids[j].x;
                float cy = candidate_centroids[j].y;
                if (cx < minX || cx > maxX || cy < minY || cy > maxY) {
                    continue;
                }
                int area = cv::contourArea(candidate_contours[j]);
                if (area < moving_blobs_max_area_threshold && area > moving_blobs_min_area_threshold) {
                    next_labels.push_back(create_new_label());
                    next_bounding_rects.push_back(candidate_bounding_rects[j]);
                    next_contours.push_back(candidate_contours[j]);
                    next_centroids.push_back(candidate_centroids[j]);
                    next_velocities.push_back(candidate_velocities[j]);
                    next_missing.push_back(0);
                    next_frame_count.push_back(1);
                }
            }
        }
    }

    uint create_new_label() {
        static uint next_label = 1;
        return next_label++;
    }

    void detect_moving_candidates(
            const cv::Mat& residualFlow,
            const cv::Mat& residualFlowIntensity,
            std::vector<std::vector <cv::Point>>&candidate_contours,
            std::vector<cv::Rect>& candidate_bounding_rects,
            std::vector<cv::Point>& candidate_centroids,
            std::vector<cv::Point>& candidate_velocities,
            const cv::Mat& motion_mask) {
        cv::Mat tempFlowIntensity;
        residualFlowIntensity.convertTo(tempFlowIntensity, CV_8UC1, 255);
        //TODO: Avaliar qual é o melhor parametro mode/method
        cv::findContours(tempFlowIntensity, candidate_contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

        for (size_t i = 0; i < candidate_contours.size(); i++) {

            cv::Moments m = cv::moments(candidate_contours[i], false);
            candidate_centroids.push_back(cv::Point(m.m10 / m.m00, m.m01 / m.m00));

            cv::Rect candidate_bounding_rect = cv::boundingRect(candidate_contours[i]);
            cv::Mat candidate_roi = residualFlow(candidate_bounding_rect);
            cv::Mat contour_mask = motion_mask(candidate_bounding_rect);
            cv::Scalar mean = cv::mean(candidate_roi, contour_mask);

            candidate_bounding_rects.push_back(candidate_bounding_rect);

            candidate_velocities.push_back(cv::Point(mean[0], mean[1]));
        }

    }

    void create_contour_bounding_rects(std::vector<std::vector <cv::Point>>&contours, std::vector<cv::Rect>& bounding_rects) {
        std::transform(contours.begin(), contours.end(), std::back_inserter(bounding_rects), [](const std::vector<cv::Point>& contour) {
            return cv::boundingRect(contour);
        });
    }

    void filter_non_object_like_blobs(std::vector<std::vector <cv::Point>>&candidate_contours, const cv::Mat stabFrame, const cv::Mat residualFlow, const int moving_blobs_min_area_threshold, const int moving_blobs_max_area_threshold) {
        // get rid of contours with area < moving_blobs_area_threshold
        // TODO: Analisar blob com graphcut e remover coisa que não parece objeto
        candidate_contours.erase(
                std::remove_if(
                candidate_contours.begin(),
                candidate_contours.end(),
                [moving_blobs_max_area_threshold, moving_blobs_min_area_threshold](std::vector<cv::Point>& candidate_contour) {
                    int area = cv::contourArea(candidate_contour);
                    if (area < moving_blobs_max_area_threshold && area > moving_blobs_min_area_threshold) {
                        if (debug_enabled) {
                            std::cout << "mantendo objeto de area " << area << std::endl;
                        }
                        return false;
                    } else {
                        if (debug_enabled) {
                            std::cout << "filtrando objeto de area " << area << std::endl;
                        }
                        return true;
                    }
                }
        ),
        candidate_contours.end()
                );
    }

    void create_ref_points(cv::Mat& referencePoints, const cv::Mat& motion_mask, const cv::Mat& image, const int maxFeatureCount, const int gridRows, const int gridCols, const int margin, const double qlevel, const double minDist) {
        int roiW = (image.cols - 2 * margin) / gridCols;
        int roiH = (image.rows - 2 * margin) / gridRows;
        int maxGridFeatureCount = maxFeatureCount / (gridCols * gridRows);
        std::vector<cv::Mat> allRp;
        for (int i = 0; i < gridCols; i++) {
            for (int j = 0; j < gridRows; j++) {
                cv::Mat rp;
                int dx = margin + i*roiW;
                int dy = margin + j*roiH;
                cv::Mat roi = image(cv::Rect(dx, dy, roiW, roiH));
                // Obtain initial set of features
                if (motion_mask.empty()) {
                    cv::goodFeaturesToTrack(roi, // the image 
                            rp, // the output detected features
                            maxGridFeatureCount,
                            qlevel, // quality level
                            minDist // min distance between two features
                            );
                } else {
                    cv::goodFeaturesToTrack(roi, // the image 
                            rp, // the output detected features
                            maxGridFeatureCount,
                            qlevel, // quality level
                            minDist, // min distance between two features
                            motion_mask(cv::Rect(dx, dy, roiW, roiH))
                            );
                }
                if (!rp.empty()) allRp.push_back(rp + cv::Scalar(dx, dy));
            }
        }
        if (!allRp.empty()) {
            cv::vconcat(allRp, referencePoints);
        }
    }

    void estimate_transform(cv::Mat& t, const cv::Mat& desired_reference_points, cv::Mat& reference_points, const cv::Mat& prevGrey, const cv::Mat& grey, const cv::Size winSize, const int maxLevel) {
        if (desired_reference_points.size().height < 8) {
            t.create(0, 0, CV_32FC1);
        } else {
            cv::Mat flowPoints;
            lk_estimate_flow_points(prevGrey, grey, desired_reference_points, reference_points, flowPoints, winSize, maxLevel);
            //TODO: Parametrizar estimate Rigid transform
            cv::Mat trans = cv::estimateRigidTransform(reference_points, flowPoints, false);
            t = trans;
        }
    }

    void lk_estimate_flow_points(const cv::Mat& prevGrey, const cv::Mat& grey, const cv::Mat& desiredReferencePoints, cv::Mat& referencePoints, cv::Mat& flowPoints, const cv::Size winSize, const int maxLevel) {
        cv::Mat status;
        cv::Mat tempFlowPts(desiredReferencePoints.rows, desiredReferencePoints.cols, desiredReferencePoints.type());
        std::vector<float> err;
        cv::TermCriteria criteria(cv::TermCriteria::COUNT + cv::TermCriteria::EPS, 10, 0.05);
        cv::calcOpticalFlowPyrLK(
                prevGrey,
                grey,
                desiredReferencePoints,
                tempFlowPts,
                status,
                err,
                winSize,
                maxLevel,
                criteria,
                cv::OPTFLOW_LK_GET_MIN_EIGENVALS
                );
        int totalValidCount = 0;
        for (int i = 0; i < status.rows; i++) {
            if (status.at<uchar>(i, 0) != 0) {
                totalValidCount++;
            }
        }
        if (totalValidCount != status.rows) {
            referencePoints.create(totalValidCount, 1, CV_32FC2);
            flowPoints.create(totalValidCount, 1, CV_32FC2);
            int currentPoint = 0;
            for (int i = 0; i < status.rows; i++) {
                if (status.at<uchar>(i, 0) != 0) {
                    const cv::Vec2f reference = desiredReferencePoints.at<cv::Vec2f>(i, 0);
                    const cv::Vec2f flowPoint = tempFlowPts.at<cv::Vec2f>(i, 0);
                    referencePoints.at<cv::Vec2f>(currentPoint, 0) = reference;
                    flowPoints.at<cv::Vec2f>(currentPoint, 0) = flowPoint;
                    currentPoint++;
                }
            }
        } else {
            referencePoints = desiredReferencePoints;
            flowPoints = tempFlowPts;
        }
    }

    void to_grey(cv::Mat& frame, cv::Mat& grey) {
        if (frame.channels() == 1) {
            grey = frame;
        } else {
            cv::cvtColor(frame, grey, CV_BGR2GRAY);
        }
    }

    void detect_moving_blobs_outside_margin(const cv::Mat& residualFlow, const cv::Mat& residualFlowIntensity, std::vector<std::vector <cv::Point>>&candidate_contours, const int moving_blobs_roi_margin) {
        cv::Point2i roiTL(moving_blobs_roi_margin, moving_blobs_roi_margin);
        cv::Rect roiRect(roiTL, cv::Size(residualFlowIntensity.cols - 2 * moving_blobs_roi_margin, residualFlowIntensity.rows - 2 * moving_blobs_roi_margin));
        cv::Mat roi = residualFlowIntensity(roiRect);
        cv::Mat tempFlowIntensity;
        roi.convertTo(tempFlowIntensity, CV_8UC1, 255);
        //TODO: Avaliar qual é o melhor parametro mode/method
        cv::findContours(tempFlowIntensity, candidate_contours, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE, roiTL);
    }

    void calculate_flow_intensity(const cv::Mat& residualFlow, cv::Mat& residualFlowIntensity) {
        //TODO: Acho que dá pra implementar fácil com cuda para otimizar
        cv::Mat flowSquared = residualFlow.mul(residualFlow);
        cv::Mat channels[2];
        cv::split(flowSquared, channels);
        cv::sqrt(channels[0] + channels[1], residualFlowIntensity);
    }

    void remove_flowintensity_noise(const cv::Mat& flowIntensity, cv::Mat& denoisedFlowIntensity, const double threshold, cv::Size openCloseKernelSize) {
        cv::Mat tempFlow;
        cv::threshold(flowIntensity, denoisedFlowIntensity, threshold, 100, cv::THRESH_TOZERO);
        if (openCloseKernelSize.height != 0) {
            cv::Mat kernel = cv::getStructuringElement(cv::MORPH_RECT, openCloseKernelSize);
            cv::morphologyEx(denoisedFlowIntensity, tempFlow, cv::MORPH_OPEN, kernel);
            cv::morphologyEx(tempFlow, denoisedFlowIntensity, cv::MORPH_CLOSE, kernel);
        }
    }

    void FlowTracker::display(
            cv::Mat& baseImage,
            const std::vector<uint>& labels,
            const std::vector<std::vector <cv::Point>>&contours,
            const std::vector<cv::Rect>& bounding_rects,
            const std::vector<cv::Point>& centroids,
            const std::vector<cv::Point>& velocities,
            const std::vector<int>& missing,
            const std::vector<int>& frame_count
            ) {
        drawMovingObjects(baseImage, labels, contours, bounding_rects, centroids, velocities, missing, frame_count);
        cv::imshow("debug", baseImage);
        if (save_image) {
            saveImage(baseImage);
        }
    }

    void drawMovingObjects(cv::Mat& baseImage, const std::vector<uint>& labels, const std::vector<std::vector <cv::Point>>&contours, const std::vector<cv::Rect>& bounding_rects, const std::vector<cv::Point>& centroids, const std::vector<cv::Point>& velocities, const std::vector<int>& missing, const std::vector<int>& frame_count) {
        if (!contours.empty()) {
            for (size_t i = 0; i < bounding_rects.size(); i++) {
                cv::Rect rect = bounding_rects[i];
                if (missing[i] > 0) {
                    cv::rectangle(baseImage, rect, cv::Scalar(127, 127, 127), 2);
                } else {
                    cv::rectangle(baseImage, rect, cv::Scalar(255, 255, 255), 2);
                }
                drawCaption(baseImage, rect.tl() + cv::Point(0, -2), std::to_string(labels[i]));
                cv::arrowedLine(
                        baseImage,
                        centroids[i],
                        centroids.at(i) + (50 * velocities.at(i)),
                        cv::Scalar(127, 127, 0));
            }
            cv::drawContours(baseImage, contours, -1, cv::Scalar(0, 0, 0), 2);
        }
    }

    void FlowTracker::debug(
            const cv::Mat& prevGrey,
            const cv::Mat& prevFrame,
            const cv::Mat& grey,
            const cv::Mat& frame,
            const cv::Mat& stabGrey,
            const cv::Mat& residualFlow,
            const cv::Mat& residualFlowIntensity,
            const cv::Mat& flowIntensity,
            const cv::Mat& trans,
            const std::vector<uint>& labels,
            const std::vector<std::vector<cv::Point>>&contours,
            const std::vector<cv::Point>& centroids,
            const std::vector<cv::Rect>& bounding_rects,
            const std::vector<cv::Point>& velocities,
            const std::vector<int>& missing,
            const std::vector<int>& frame_count,
            const std::vector<std::vector <cv::Point>>&candidate_contours,
            const std::vector<cv::Rect>& candidate_bounding_rects,
            const std::vector<cv::Point>& candidate_centroids,
            const std::vector<cv::Point>& candidate_velocities) {

        cv::Mat combined_mat(grey.rows * 2, grey.cols * 2, CV_8UC3);

        cv::Rect grey_image_rect(0, 0, grey.cols, grey.rows);
        cv::Rect stab_diff_rect(grey.size().width, 0, grey.cols, grey.rows);
        cv::Rect flow_image_rect(0, grey.size().height, grey.cols, grey.rows);
        cv::Rect motion_image_rect(grey.size().width, grey.size().height, grey.cols, grey.rows);


        cv::Mat grey_image(combined_mat, grey_image_rect);
        cv::Mat stab_diff(combined_mat, stab_diff_rect);
        cv::Mat flow_image(combined_mat, flow_image_rect);
        cv::Mat motion_image(combined_mat, motion_image_rect);

        //grey
        cv::cvtColor(prevGrey, grey_image, CV_GRAY2RGB);
        drawMotion(trans, residualFlow, grey_image, 5, cv::Scalar(0, 255, 0), 1);
        drawMovingObjects(grey_image, labels, contours, bounding_rects, centroids, velocities, missing, frame_count);

        //stab diff
        drawStabDiff(prevGrey, stabGrey, stab_diff);
        drawMovingObjects(stab_diff, labels, contours, bounding_rects, centroids, velocities, missing, frame_count);

        drawFlowIntensity(flowIntensity, flow_image, true, true, true);
        drawMovingObjects(flow_image, labels, contours, bounding_rects, centroids, velocities, missing, frame_count);

        if (transformSeemsValid(trans)) {
            drawFlowIntensity(residualFlowIntensity, motion_image, true, true, true);
            drawMovingObjects(motion_image, labels, contours, bounding_rects, centroids, velocities, missing, frame_count);
        }

        //add captions
        cv::Point2i captionPoint(10, 20);
        drawRoiCaption(grey_image, grey_image_rect, captionPoint, "Grey");
        drawRoiCaption(stab_diff, stab_diff_rect, captionPoint, "Stab Diff");
        drawRoiCaption(flow_image, flow_image_rect, captionPoint, "Flow");
        drawRoiCaption(motion_image, motion_image_rect, captionPoint, "Motion");

        //display transform info
        cv::putText(combined_mat, transDescription(trans), cv::Point2i(20, 80), cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, cv::Scalar(200, 200, 250), 1, CV_AA);

        //render image
        cv::imshow("debug", combined_mat);

        if (save_image) {
            saveImage(combined_mat);
        }

    }

    cv::Mat& drawMotion(const cv::Mat transform, const cv::Mat& residual_flow, cv::Mat& motion_image, int step, const cv::Scalar& color, double threshold) {
        if (transformSeemsValid(transform)) {
            return drawDenseFlow(residual_flow, step, color, motion_image, threshold);
        } else {
            return motion_image;
        }
    }

    cv::Mat& drawStabDiff(const cv::Mat& prevGrey, const cv::Mat& stab_grey, cv::Mat& stab_diff) {
        cv::cvtColor(prevGrey - stab_grey, stab_diff, CV_GRAY2BGR);
        return stab_diff;
    }

    bool transformSeemsValid(const cv::Mat& trans) {
        if (trans.empty()) {
            return false;
        } else {
            //        const double angle = atan2(trans.at<double>(1, 0), trans.at<double>(0, 0)) * 180 / M_PI;
            //        const double dx = trans.at<double>(0, 2);
            //        const double dy = trans.at<double>(1, 2);
            //        return angle <= 20 && abs(dx) < 20 && abs(dy) < 20;
            return true;
        }
    }

    cv::Mat& drawDenseFlow(const cv::Mat& flow, int step, const cv::Scalar& color, cv::Mat& flow_image, double threshold) {
        for (int y = 5; y < flow_image.rows; y += step) {
            for (int x = 5; x < flow_image.cols; x += step) {
                const cv::Vec2f& m = flow.at<cv::Vec2f>(y, x);
                if (cv::norm(m) >= threshold) {
                    cv::Point src_point(x, y);
                    cv::Point dest_point(cvRound(x + m[0]), cvRound(y + m[1]));
                    cv::line(flow_image, src_point, dest_point, color);
                    cv::circle(flow_image, src_point, 1, CV_RGB(255, 0, 0), -1);
                }
            }
        }
        return flow_image;
    }

    void drawRoiCaption(cv::Mat& roi, const cv::Rect roiRect, const cv::Point& p, const std::string& caption) {
        cv::putText(roi, caption, roiRect.tl() + p, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, CV_RGB(0, 0, 255), 1, CV_AA);
    }

    void drawCaption(cv::Mat& image, const cv::Point& p, const std::string& caption) {
        cv::putText(image, caption, p, cv::FONT_HERSHEY_COMPLEX_SMALL, 0.8, CV_RGB(255, 0, 0), 1, CV_AA);
    }

    std::string transDescription(const cv::Mat& trans) {
        if (!transformSeemsValid(trans)) {
            return "Couldn't calculate transform!";
        }
        double angle = atan2(trans.at<double>(1, 0), trans.at<double>(0, 0)) * 180 / M_PI;
        double dx = trans.at<double>(0, 2);
        double dy = trans.at<double>(1, 2);
        return boost::str(boost::format("Rotate: %1.2f dx: %2.2f dy: %3.2f") % angle % dx % dy);
    }

    cv::Mat& drawFlowIntensity(const cv::Mat& flowIntensity, cv::Mat& flow_image, bool normalization, bool equalization, bool applyColormap) {
        cv::Mat flowIntensityImage(flowIntensity.size(), CV_8UC1);
        cv::Mat tempFlow;
        if (normalization) {
            cv::normalize(flowIntensity, tempFlow, 1, 0, cv::NORM_MINMAX);
            tempFlow.convertTo(flowIntensityImage, CV_8UC1, 255);
        } else {
            flowIntensity.convertTo(flowIntensityImage, CV_8UC1, 255);
        }
        if (equalization) {
            cv::equalizeHist(flowIntensityImage, tempFlow);
            flowIntensityImage = tempFlow;
        }
        if (applyColormap) {
            cv::applyColorMap(flowIntensityImage, flow_image, cv::COLORMAP_JET);
        } else {
            flowIntensityImage.copyTo(flow_image);
        }
        return flow_image;
    }

    void drawReferencePoints(const cv::Mat& referencePoints, const cv::Scalar& color, cv::Mat& flow_image) {
        for (int i = 0; i < referencePoints.rows; i++) {
            cv::circle(flow_image, referencePoints.at<cv::Point2f>(i, 0), 1, CV_RGB(255, 0, 0), -1);
        }
    }

    //    void arrowedLine(cv::Mat& img, cv::Point pt1, cv::Point pt2, const cv::Scalar& color,
    //            int thickness, int lineType, int shift, double normalizationFactor) {
    //        const double tipSize = cv::norm(pt1 - pt2) / normalizationFactor; // Factor to normalize the size of the tip depending on the length of the arrow
    //
    //        cv::line(img, pt1, pt2, color, thickness, lineType, shift);
    //
    //        const double angle = atan2((double) pt1.y - pt2.y, (double) pt1.x - pt2.x);
    //
    //        cv::Point p((int) (pt2.x + tipSize * cos(angle + CV_PI / 4)),
    //                (int) (pt2.y + tipSize * sin(angle + CV_PI / 4)));
    //        cv::line(img, p, pt2, color, thickness, lineType, shift);
    //
    //        p.x = (int) (pt2.x + tipSize * cos(angle - CV_PI / 4));
    //        p.y = (int) (pt2.y + tipSize * sin(angle - CV_PI / 4));
    //        cv::line(img, p, pt2, color, thickness, lineType, shift);
    //    }

    void debug(const std::string msg, const cv::Mat& m) {
        std::cout << msg << std::endl;
        std::cout << m << std::endl;
    }

    template <class type> void debug(const std::string msg, const std::vector<type>& v) {
        std::cout << msg << std::endl;
        for (type e : v)
            std::cout << e << ',' << ' ';
        std::cout << std::endl;
    }
}