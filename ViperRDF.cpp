/* 
 * File:   ViperRDF.cpp
 * Author: leonardo
 * 
 * Created on August 2, 2014, 10:03 PM
 */

#include "ViperRDF.h"
#include <fstream>
#include <streambuf>
#include <string>
#include <cerrno>

ViperRDF::ViperRDF(std::string viper_mustache_template_file, std::string source, uint framerate) :
viper_mustache_template_file(viper_mustache_template_file),
source(source),
framerate(framerate),
numframes(0),
framespan_format(boost::format("%1%:%2%")),
tracks(std::map<uint, std::vector<bounding_box>>()),
visibility_info(std::map<uint,std::vector<visibility>>()) {
}

ViperRDF::ViperRDF(const ViperRDF& orig) {
}

ViperRDF::~ViperRDF() {
}

void ViperRDF::addFrameTracks(std::vector<uint>& labels, std::vector<cv::Rect>& bounding_boxes, std::vector<int>& missing) {
    numframes++;
    for (size_t i = 0; i < labels.size(); i++) {
        //if (missing[i] == 0) {
            bounding_box b = {numframes, bounding_boxes[i]};
            tracks[labels[i]].push_back(b);
        //}
        
        if (visibility_info.count(labels[i]) == 0 || visibility_info[labels[i]].back().closed) {
            //Se for um novo ou registro já terminado, inicia novo registro
            visibility_info[labels[i]].push_back({numframes, numframes, missing[i]==0, false});
        } else {
            //se trocou o valor de missing, fecha o registro e adiciona novo, senão, não faz nada
            if ((missing[i] == 0) != visibility_info[labels[i]].back().visible) {
                //atualiza frame final
                visibility_info[labels[i]].back().end_frame = numframes-1;
                visibility_info[labels[i]].back().closed = true;
                visibility_info[labels[i]].push_back({numframes, numframes, missing[i]==0, false});
            }
            visibility_info[labels[i]].back().end_frame = numframes;
            
        }
    }

}

void ViperRDF::writeTo(std::string filename) {

    ctemplate::TemplateDictionary viper("viper");

    create_viper_ctemplate_dictionary(viper);

    std::string output;

    ctemplate::ExpandTemplate(viper_mustache_template_file, ctemplate::DO_NOT_STRIP, &viper, &output);

    std::ofstream ofs(filename, std::ofstream::out);

    ofs << output;

    ofs.close();
}

void ViperRDF::create_viper_ctemplate_dictionary(ctemplate::TemplateDictionary& viper) {

    //seção sourcefile
    //{{sourcefile}}, {{NUMFRAMES}}, {{FRAMERATE}}
    viper["sourcefile"] = source;
    viper["numframes"] = numframes+1;
    viper["framerate"] = framerate;

    //FRAME object
    //"{{FrameFrameSpan}}" framespan=1:1789
    viper["framespan"] = boost::str(framespan_format % 1 % (numframes+1));
    for (const auto &track : tracks) {
        ctemplate::TemplateDictionary *object = viper.AddSectionDictionary("objects");
        object->SetIntValue("id", track.first);
        object->SetValue("framespan",
                boost::str(framespan_format % visibility_info[track.first].front().start_frame % visibility_info[track.first].back().end_frame)
                );

        for (const auto frame : track.second) {
            ctemplate::TemplateDictionary *bounding_box = object->AddSectionDictionary("bounding_box");
            bounding_box->SetIntValue("frame", frame.frame);
            bounding_box->SetIntValue("height", frame.rect.height);
            bounding_box->SetIntValue("width", frame.rect.width);
            bounding_box->SetIntValue("x", frame.rect.x);
            bounding_box->SetIntValue("y", frame.rect.y);
        }
        for (const auto info : visibility_info[track.first]) {
            ctemplate::TemplateDictionary *bounding_box = object->AddSectionDictionary("visibility_info");
            bounding_box->SetIntValue("start_frame", info.start_frame);
            bounding_box->SetIntValue("end_frame", info.end_frame);
            bounding_box->SetValue("visible", info.visible ? "true" : "false");
        }
    }


    //objetos detectados
    //    <object framespan="51:148" id="18" name="VEHICLE"> 
    //        <attribute name="LOCATION"> 
    //            <data:obox framespan="51:51" height="20" rotation="0" width="16" x="142" y="56" /> 
    //            <data:obox framespan="52:52" height="20" rotation="0" width="15" x="140" y="56" /> 
    //            <data:obox framespan="53:53" height="20" rotation="0" width="20" x="135" y="56" /> 
    //            <data:obox framespan="54:54" height="20" rotation="0" width="20" x="133" y="56" /> 
    //        </attribute> 
    //    </object>     
}

std::string get_file_contents(const std::string& filename) {
    std::ifstream in(filename, std::ios::in | std::ios::binary);
    if (in) {
        return (std::string((std::istreambuf_iterator<char>(in)), std::istreambuf_iterator<char>()));
    }
    throw (errno);
}