/* 
 * File:   hungarian_association.h
 * Author: leonardo
 *
 * Created on August 2, 2014, 11:35 AM
 */

#ifndef HUNGARIAN_ASSOCIATION_H
#define	HUNGARIAN_ASSOCIATION_H

#include <opencv2/core.hpp>
#include <cstdio>
#include <iostream>
#include <map>
#include <set>
#include <vector>
#include <tuple>

namespace tracker {

    void subtract_dim(cv::Mat& inout, cv::Mat& min, int dim);

    void hungarian_association(const cv::Mat& assigncost, std::map<int, int>& results);

    void hungarian_prepare(const cv::Mat& assigncost, cv::Mat& costs);

    void hungarian_init_stars(cv::Mat& costs, std::vector<int>& starts_by_rows, std::vector<int>& starts_by_cols);

    void hungarian_cover_cols_of_starred_zeroes(std::vector<int>& starts_by_cols, std::vector<bool>& covered_cols);

    bool hungarian_all_are_covered(std::vector<bool>& covered_cols);

    std::tuple<int, int> hungarian_prime_uncovered_zero(cv::Mat& costs, std::vector<int>& primes_by_rows, std::vector<bool>& covered_rows, std::vector<bool>& covered_cols);

    void hungarian_make_more_zeroes(cv::Mat& costs, std::vector<bool>& covered_rows, std::vector<bool>& covered_cols);

    void hungarian_increment_set_of_starred_zeroes(std::tuple<int, int> unpairedZeroPrime, std::vector<int>& starsByRow, std::vector<int>& starsByCol, std::vector<int>& primesByRow);
    
    template<typename the_type> void debug_vector(std::string var_name, std::vector<the_type> the_vector);

}

#endif	/* HUNGARIAN_ASSOCIATION_H */

