/* 
 * File:   DirectoryImageSource.h
 * Author: leonardo
 *
 * Created on July 19, 2014, 9:35 AM
 */

#ifndef DIRECTORYIMAGESOURCE_H
#define	DIRECTORYIMAGESOURCE_H

#include "ImageSource.h"

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>

namespace fs = ::boost::filesystem;

namespace tracker {

    class DirectoryImageSource : public ImageSource {
    public:
        DirectoryImageSource(fs::path& dir, std::string& ext, int start_frame, uint max_frames);
        DirectoryImageSource(const DirectoryImageSource& orig);
        virtual ~DirectoryImageSource();
        virtual cv::Mat next();
        virtual uint count();
        virtual std::string get_current_filename();
        virtual uint get_current_index();
    private:
        const std::vector<std::string> image_files;
        uint i;
        int start_frame;
        uint max_frames;
        uint end_frame;
    };
}
#endif	/* DIRECTORYIMAGESOURCE_H */

