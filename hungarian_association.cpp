
#include <opencv2/core/core.hpp>
#include "hungarian_association.h"
#include "FlowTracker.h"

namespace tracker {

    const int DIM_COLS = 0;
    const int DIM_ROWS = 1;
    const float EPSILON = 0.0000000000000000000000000001f;

    void subtract_dim(cv::Mat& inout, cv::Mat& min, int dim) {
        if (dim == DIM_COLS) {
            for (int i = 0; i < inout.cols; i++) {
                inout.col(i) -= min.at<float>(0, i);
            }
        } else {
            for (int i = 0; i < inout.cols; i++) {
                inout.row(i) -= min.at<float>(i, 0);
            }
        }
    }

    /**
     * http://www.ee.oulu.fi/~mpa/matreng/eem1_2-1.htm
     * @param assigncost
     * @param rowsol
     * @param colsol
     * @param u
     * @param v
     */
    void hungarian_association(const cv::Mat& assigncost, std::map<int, int>& results) {

        int iteration = 0;
        
        cv::Mat costs;

        if (debug_enabled) {
            //std::cout << "assigncosts" << std::endl << assigncost << std::endl;
        }

        hungarian_prepare(assigncost, costs);

        if (debug_enabled) {
            //std::cout << "costs" << std::endl << costs << std::endl;
        }

        int dim = costs.rows;

        std::vector<int> stars_by_rows(dim, -1);
        std::vector<int> stars_by_cols(dim, -1);
        std::vector<int> primes_by_rows(dim, -1);

        // 1s mean covered, 0s mean not covered
        std::vector<bool> covered_rows(dim, false);
        std::vector<bool> covered_cols(dim, false);

        hungarian_init_stars(costs, stars_by_rows, stars_by_cols);

        hungarian_cover_cols_of_starred_zeroes(stars_by_cols, covered_cols);

//        if (debug_enabled) {
//            std::cout << "After init" << std::endl;
//            //std::cout << "costs" << std::endl << costs << std::endl;
//            debug_vector("primes_by_rows", primes_by_rows);
//            debug_vector("covered rows", covered_rows);
//            debug_vector("stars_by_cols", stars_by_cols);
//            debug_vector("covered cols", covered_cols);
//        }
        //Enquanto solução otima não encontrada
        while (!hungarian_all_are_covered(covered_cols)) {
//            if (debug_enabled) {
//                std::cout << "Not all covered!" << std::endl;
//            }
            int primed_zero_col = -1;
            int primed_zero_row = -1;

            if (debug_enabled) {
                //std::cout << "costs" << std::endl << costs << std::endl;
                std::cout << iteration++ << std::endl;
//                debug_vector("primes_by_rows", primes_by_rows);
//                debug_vector("covered rows", covered_rows);
//                debug_vector("stars_by_cols", stars_by_cols);
//                debug_vector("covered cols", covered_cols);
//                std::cout << std::endl << std::endl;
            }
            std::tuple<int, int> zero_prime = hungarian_prime_uncovered_zero(costs, primes_by_rows, covered_rows, covered_cols);
            std::tie(primed_zero_row, primed_zero_col) = zero_prime;

            while (primed_zero_row == -1 && primed_zero_col == -1) {
                // keep making more zeroes until we find something that we can prime (i.e. a zero that is uncovered)
                hungarian_make_more_zeroes(costs, covered_rows, covered_cols);
                zero_prime = hungarian_prime_uncovered_zero(costs, primes_by_rows, covered_rows, covered_cols);
                std::tie(primed_zero_row, primed_zero_col) = zero_prime;
            }

            // check if there is a starred zero in the primed zero's row
            int columnIndex = stars_by_rows.at(primed_zero_row);
            if (-1 == columnIndex) {
                // if not, then we need to increment the zeroes and start over
                hungarian_increment_set_of_starred_zeroes(zero_prime, stars_by_rows, stars_by_cols, primes_by_rows);
                std::fill(primes_by_rows.begin(), primes_by_rows.end(), -1);
                std::fill(covered_rows.begin(), covered_rows.end(), false);
                std::fill(covered_cols.begin(), covered_cols.end(), false);
                hungarian_cover_cols_of_starred_zeroes(stars_by_cols, covered_cols);
            } else {
                // cover the row of the primed zero and uncover the column of the starred zero in the same row
                covered_rows.at(primed_zero_row) = true;
                covered_cols.at(columnIndex) = false;
            }
        }

        // ok now we should have assigned everything
        // take the starred zeroes in each column as the correct assignments
        for (size_t i = 0; i < stars_by_cols.size(); i++) {
            if (i < assigncost.cols && stars_by_cols.at(i) < assigncost.rows) {
                results[stars_by_cols.at(i)] = i;
            }
        }      
    }

    void hungarian_prepare(const cv::Mat& assigncost, cv::Mat& costs) {
        //Torna a matrix quadrada acrescentando linhas ou colunas com custo máximo
        if (assigncost.cols > assigncost.rows) {
            double min; double max;
            cv::Point minLoc; cv::Point maxLoc;
            cv::minMaxLoc(assigncost, &min, &max, &minLoc, &maxLoc, cv::Mat());
            float maxPlusOne = (float) max + 1;
            cv::Mat complement_mat = cv::Mat(assigncost.cols - assigncost.rows, assigncost.cols, CV_32FC1, maxPlusOne);
            cv::vconcat(assigncost, complement_mat, costs);
            if (debug_enabled) {
                std::cout << costs << std::endl;
            }
        } else if (assigncost.cols < assigncost.rows) {
            double min; double max;
            cv::Point minLoc; cv::Point maxLoc;
            cv::minMaxLoc(assigncost, &min, &max, &minLoc, &maxLoc, cv::Mat());
            float maxPlusOne = (float) max + 1;
            cv::Mat complement_mat = cv::Mat(assigncost.rows, assigncost.rows - assigncost.cols, CV_32FC1, maxPlusOne);
            cv::hconcat(assigncost, complement_mat, costs);
            if (debug_enabled) {
                std::cout << costs << std::endl;
            }
        } else {
            assigncost.copyTo(costs);
        }


        //Passo 1: subtrai o mínimo de cada linha

        //Descobre o mínimo de cada linha, retornando matrix de 1 única coluna
        cv::Mat rowMinimum;
        cv::reduce(costs, rowMinimum, 1, 3 /* CV_REDUCE_MIN */);

        subtract_dim(costs, rowMinimum, DIM_ROWS);

        if (debug_enabled) {
            //std::cout << costs << std::endl;
        }
        //Passo 2: subtrai o mínimo de cada coluna

        //Descobre o mínimo de cada coluna, retornando matrix de 1 única linha
        cv::Mat colMinimum;
        cv::reduce(costs, colMinimum, 0,  3 /* CV_REDUCE_MIN */);

        subtract_dim(costs, colMinimum, DIM_COLS);

        if (debug_enabled) {
            std::cout << costs << std::endl;
        }

    }

    void hungarian_init_stars(cv::Mat& costs, std::vector<int>& starts_by_rows, std::vector<int>& starts_by_cols) {
        std::vector<int> row_has_starred_zero(costs.rows, 0);
        std::vector<int> col_has_starred_zero(costs.cols, 0);

        for (int i = 0; i < costs.rows; i++) {
            for (int j = 0; j < costs.cols; j++) {
                if (EPSILON > costs.at<float>(i, j) && 0 == row_has_starred_zero.at(i) && 0 == col_has_starred_zero.at(j)) {
                    starts_by_rows.at(i) = j;
                    starts_by_cols.at(j) = i;
                    row_has_starred_zero.at(i) = 1;
                    col_has_starred_zero.at(j) = 1;
                    break; // move onto the next row
                }
            }
        }
    }

    void hungarian_cover_cols_of_starred_zeroes(std::vector<int>& starts_by_cols, std::vector<bool>& covered_cols) {
        for (size_t i = 0; i < starts_by_cols.size(); i++) {
            covered_cols.at(i) = -1 != starts_by_cols.at(i);
        }
    }

    bool hungarian_all_are_covered(std::vector<bool>& covered_cols) {
        for (bool covered : covered_cols) {
            if (!covered) {
                return false;
            }
        }
        return true;
    }

    std::tuple<int, int> hungarian_prime_uncovered_zero(cv::Mat& costs, std::vector<int>& primes_by_rows, std::vector<bool>& covered_rows, std::vector<bool>& covered_cols) {
        // find an uncovered zero and prime it
        for (int row = 0; row < costs.rows; row++) {
            if (covered_rows.at(row)) continue;
            for (int col = 0; col < costs.cols; col++) {
                // if it's a zero and the column is not covered
                if (EPSILON > abs(costs.at<float>(row, col)) && !covered_cols.at(col)) {
                    // ok this is an unstarred zero
                    // prime it
                    primes_by_rows.at(row) = col;
                    return std::tuple<int, int>(row, col);
                }
            }
        }
        return std::tuple<int, int>(-1, -1);
    }

    void hungarian_make_more_zeroes(cv::Mat& costs, std::vector<bool>& covered_rows, std::vector<bool>& covered_cols) {
        // find the minimum uncovered value
        float min_uncovered_val = FLT_MAX;
        for (int i = 0; i < costs.rows; i++) {
            if (!covered_rows.at(i)) {
                for (int j = 0; j < costs.cols; j++) {
                    if (!covered_cols.at(j) && costs.at<float>(i, j) < min_uncovered_val) {
                        min_uncovered_val = costs.at<float>(i, j);
                    }
                }
            }
        }
        if (debug_enabled) {
            std::cout << "min uncovered val " << min_uncovered_val << std::endl;

            //std::cout << "costs antes: " << std::endl << costs << std::endl;
        }
        // add the min value to all covered rows
        for (size_t i = 0; i < covered_rows.size(); i++) {
            if (covered_rows.at(i)) {
                for (int j = 0; j < costs.cols; j++) {
                    costs.at<float>(i, j) += min_uncovered_val;
                }
            }
        }

        // subtract the min value from all uncovered columns
        for (size_t i = 0; i < covered_cols.size(); i++) {
            if (!covered_cols.at(i)) {
                for (int j = 0; j < costs.rows; j++) {
                    costs.at<float>(j, i) -= min_uncovered_val;
                }
            }
        }
        if (debug_enabled) {
            std::cout << "costs depois: " << std::endl << costs << std::endl;
        }
    }

    void hungarian_increment_set_of_starred_zeroes(std::tuple<int, int> unpairedZeroPrime, std::vector<int>& starsByRow, std::vector<int>& starsByCol, std::vector<int>& primesByRow) {

        // build the alternating zero sequence (prime, star, prime, star, etc)
        int i, j = std::get<1>(unpairedZeroPrime);
        std::set<std::tuple<int, int>> zero_sequence;
        zero_sequence.insert(unpairedZeroPrime);
        bool paired = false;
        bool already_contains = false;
        do {
            i = starsByCol.at(j);
            //std::cout << "RECEBIDO"<<i<<","<<j<<std::endl;
            already_contains = zero_sequence.count(std::tuple<int, int>(i, j)) != 0;
            paired = -1 != i && !already_contains;
            if (-1 != i && !already_contains) {
                zero_sequence.insert(std::tuple<int, int>(i, j));
            }
            if (!paired) {
                //std::cout << "Nao pareado "<<i<<","<<j<<std::endl;
                break;
            } else {
                //std::cout << "Pareado "<<i<<","<<j<<std::endl;
            }
            j = primesByRow.at(i);
            already_contains = zero_sequence.count(std::tuple<int, int>(i, j)) != 0;
            paired = -1 != j && !already_contains;
            if (-1 != j && !already_contains) {
                zero_sequence.insert(std::tuple<int, int>(i, j));
            }
            if (!paired) {
                //std::cout << "Nao pareado "<<i<<","<<j<<std::endl;
                break;
            } else {
                //std::cout << "Pareado "<<i<<","<<j<<std::endl;
            }            
        } while (paired);


        // unstar each starred zero of the sequence
        // and star each primed zero of the sequence
        int row, col;
        for (std::tuple<int, int> zero : zero_sequence) {
            std::tie(row, col) = zero;
            if (starsByCol.at(col) == row) {
                //std::cout << "Unstar "<<row<<","<<col<<std::endl;
                starsByCol.at(col) = -1;
                starsByRow.at(row) = -1;
            }
            if (primesByRow.at(row) == col) {
                starsByRow.at(row) = col;
                starsByCol.at(col) = row;
            }
        }

    }

    template<typename the_type> void debug_vector(std::string var_name, std::vector<the_type> the_vector) {
        std::cout << var_name << std::endl;
        for (auto c : the_vector)
            std::cout << c << ',';
        std::cout << std::endl;
    }

}
