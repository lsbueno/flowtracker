/* 
 * File:   ViperRDF.h
 * Author: leonardo
 *
 * Created on August 2, 2014, 10:03 PM
 */

#ifndef VIPERRDF_H
#define	VIPERRDF_H
#include <string>
#include <vector>
#include <map>
#include <tuple>
#include <boost/format.hpp>
#include <opencv2/core/core.hpp>
#include <ctemplate/template.h> 

typedef struct visibility_t {
  uint start_frame;
  uint end_frame;
  bool visible;
  bool closed;
} visibility;

typedef struct bounding_box_t {
  uint frame;
  cv::Rect rect;
} bounding_box;

class ViperRDF {
public:

    ViperRDF(std::string viper_mustache_template_file, std::string source, uint framerate = 1);

    ViperRDF(const ViperRDF& orig);

    virtual ~ViperRDF();

    void writeTo(std::string filename);

    void addFrameTracks(std::vector<uint>& labels, std::vector<cv::Rect>& bounding_boxes, std::vector<int>& missing);

private:

    std::string viper_mustache_template_file;
    
    void create_viper_ctemplate_dictionary(ctemplate::TemplateDictionary& viper);
    
    std::string source;
    
    uint framerate;
        
    uint numframes;
    
    boost::format framespan_format;
    
    std::map<uint,std::vector<bounding_box>> tracks;
    //Segundo elemento da tupla é int e não uint para podermos fazer -1 = registro não completo
    std::map<uint,std::vector<visibility>> visibility_info;
};

#endif	/* VIPERRDF_H */

