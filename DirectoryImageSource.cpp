/* 
 * File:   DirectoryImageSource.cpp
 * Author: leonardo
 * 
 * Created on July 19, 2014, 9:35 AM
 */

#include "DirectoryImageSource.h"
#include <opencv2/highgui/highgui.hpp>

namespace fs = ::boost::filesystem;

namespace tracker {

    // return the filenames of all files that have the specified extension
    // in the specified directory and all subdirectories

    std::vector<std::string> list_files(const fs::path& root, const std::string& ext) {
        std::vector<std::string> ret;
        if (!fs::exists(root)) return ret;

        if (fs::is_directory(root)) {
            fs::directory_iterator it(root);
            fs::directory_iterator endit;
            while (it != endit) {
                if (fs::is_regular_file(*it)) {
                    ret.push_back(fs::absolute((*it).path(), root).string());
                }
                ++it;
            }
        }
        std::sort(ret.begin(), ret.end());
        return ret;
    }

    DirectoryImageSource::DirectoryImageSource(fs::path& dir, std::string& ext, int start_frame, uint max_frames) : image_files(list_files(dir, ext)), i(0), start_frame(start_frame), max_frames(max_frames), end_frame(0) {
        if (start_frame < 0) {
            this->start_frame = image_files.size() + start_frame + 1;
        } else if (start_frame == 0) {
            this->start_frame = 1;
        }
        i = this->start_frame-1;
        if (max_frames == 0) {
            end_frame = image_files.size();
            this->max_frames = image_files.size();
        } else {
            end_frame = std::min((int)(start_frame + max_frames), (int)image_files.size());
        }
        
    }

    DirectoryImageSource::DirectoryImageSource(const DirectoryImageSource& orig) : image_files(orig.image_files), i(orig.i), start_frame(orig.start_frame), max_frames(orig.max_frames) {
    }

    DirectoryImageSource::~DirectoryImageSource() {
    }

    cv::Mat DirectoryImageSource::next() {
        if (i < end_frame) {
            return cv::imread(image_files[i++]);
        } else {
            return cv::Mat();
        }
    }
    
    uint DirectoryImageSource::count() {
        return max_frames;
    }
    
    std::string DirectoryImageSource::get_current_filename() {
        return image_files[i];
    }
    
    uint DirectoryImageSource::get_current_index() {
        return i;
    }
}