#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux-x86
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/DirectoryImageSource.o \
	${OBJECTDIR}/FlowTracker.o \
	${OBJECTDIR}/ViperRDF.o \
	${OBJECTDIR}/hungarian_association.o


# C Compiler Flags
CFLAGS=-m64

# CC Compiler Flags
CCFLAGS=-m64 -std=c++0x
CXXFLAGS=-m64 -std=c++0x

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-L/usr/local/cuda/lib64 -lboost_filesystem -lboost_system -lboost_program_options `pkg-config --libs opencv` /usr/lib/libctemplate_nothreads.so  

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libflowtracker.${CND_DLIB_EXT}

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libflowtracker.${CND_DLIB_EXT}: /usr/lib/libctemplate_nothreads.so

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libflowtracker.${CND_DLIB_EXT}: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libflowtracker.${CND_DLIB_EXT} ${OBJECTFILES} ${LDLIBSOPTIONS} -Wl,--no-undefined -shared -fPIC

${OBJECTDIR}/DirectoryImageSource.o: nbproject/Makefile-${CND_CONF}.mk DirectoryImageSource.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/local/cuda-6.5/include `pkg-config --cflags opencv`   -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/DirectoryImageSource.o DirectoryImageSource.cpp

${OBJECTDIR}/FlowTracker.o: nbproject/Makefile-${CND_CONF}.mk FlowTracker.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/local/cuda-6.5/include `pkg-config --cflags opencv`   -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/FlowTracker.o FlowTracker.cpp

${OBJECTDIR}/ViperRDF.o: nbproject/Makefile-${CND_CONF}.mk ViperRDF.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/local/cuda-6.5/include `pkg-config --cflags opencv`   -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/ViperRDF.o ViperRDF.cpp

${OBJECTDIR}/hungarian_association.o: nbproject/Makefile-${CND_CONF}.mk hungarian_association.cpp 
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -Wall -I/usr/local/cuda-6.5/include `pkg-config --cflags opencv`   -fPIC  -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/hungarian_association.o hungarian_association.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libflowtracker.${CND_DLIB_EXT}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
